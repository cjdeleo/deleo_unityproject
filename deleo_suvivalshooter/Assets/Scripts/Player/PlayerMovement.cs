﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;

    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100f;

    public GameObject deathcube1;
    public GameObject deathcube2;
    public GameObject deathcube3;
   
    bool PickedUp = false;

    void Awake () {
        floorMask = LayerMask.GetMask ("Floor");
        anim = GetComponent<Animator> ();
        playerRigidbody = GetComponent<Rigidbody> ();
    }
    
    void FixedUpdate () {
        float h = Input.GetAxisRaw ("Horizontal");
        float v = Input.GetAxisRaw ("Vertical");

        Move (h, v);
        Turning ();
        Animating (h, v);
    }

    void Move (float h, float v) {
        movement.Set (h, 0f, v);

        movement = movement.normalized * speed * Time.deltaTime;

        playerRigidbody.MovePosition (transform.position + movement);
    }

    void Turning () {
        Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);

        RaycastHit floorHit;

        if (Physics.Raycast (camRay, out floorHit, camRayLength, floorMask)) {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation (playerToMouse);
            playerRigidbody.MoveRotation (newRotation);
        }
    }

    void Animating (float h, float v) {
        bool walking = h != 0 || v != 0f;
        anim.SetBool ("IsWalking", walking);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Pickup")){
            EnemyHealth[] enemyHealths = FindObjectsOfType<EnemyHealth>();
            foreach (EnemyHealth eH in enemyHealths){
                eH.Death ();
            }
            PickedUp = true;
            Destroy(other.gameObject);
            Invoke("NewCube", 60);
        }
    }

    void NewCube()
    {
        Vector3 position1 = new Vector3(-10.6f, 0.5f, -8.32f);
        Instantiate(deathcube1, position1, Quaternion.identity);

        Vector3 position2 = new Vector3(-25.44f, 0.5f, 5.77f);
        Instantiate(deathcube2, position2, Quaternion.identity);

        Vector3 position3 = new Vector3(20.97f, 0.5f, -0.14f);
        Instantiate(deathcube3, position3, Quaternion.identity);
    }
}